# OUROBOROS

Docker image permettant de mettre à jour automatiquement des conteneurs Docker dès que leur image d'origine est updaté sur le registre

## CONFIGURATION

```bash
nano ouroboros.env
```

> Modifier les conteneurs ignorés ainsi que l'interval de vérification de Ouroboros

```bash
docker login monregistre.mondomaine.tld
```

## UTILISATION

```bash
docker compose up -d
```